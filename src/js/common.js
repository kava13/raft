$(function () {
  //слайдеры
  let sliderArrow = {
    prev:
      '<div class="arrow-slider a-prev"><img src="/src/img/arrow-left.svg" alt="" srcset="" /></div>',
    next:
      '<div class="arrow-slider a-next"><img src="/src/img/arrow-left.svg" alt="" srcset="" /></div>',
  };

  //сдайдер
  $(".hero-slider__list").slick({
    infinite: true,
    prevArrow: sliderArrow.prev,
    nextArrow: sliderArrow.next,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          variableWidth: true,
        },
      },
    ],
  });

  $(".assortment-slider__list").slick({
    infinite: true,
    prevArrow: sliderArrow.prev,
    nextArrow: sliderArrow.next,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          variableWidth: true,
        },
      },
    ],
  });
});
