const path = require('path');
const webpack = require('webpack');
const fs = require('fs');
// webpack plugins
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CtrlwebHtmlWebpackLayoutPlugin = require('ctrlweb-html-webpack-layout');
const mqpacker  = require('css-mqpacker');
const cssnano  = require('cssnano');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const sortCSSmq = require('sort-css-media-queries');

// Если работаем в продакшн режиме, то генерим файл
const isProduction = process.env.NODE_ENV === 'production';
// layout
const layoutPath = path.join(__dirname, '/src/html/layouts/main-layout.html');
const layoutOrder = path.join(__dirname, '/src/html/layouts/layout-order.html');


// load partials from file to string
function getPartialContent(pathToPartial) {
    return fs.readFileSync(pathToPartial, 'utf-8');
}

const templateReplaceAssets = {
    'breadcrumbs': getPartialContent('src/html/partials/breadcrumbs.html'),
    'smartfilter': getPartialContent('src/html/partials/smartfilter.html'),
    'menu': getPartialContent('src/html/partials/menu.html'),
};

function generateHtmlPlugins(templateDir) {
    const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir));
    return templateFiles.map(item => {
        const parts = item.split('.');
        const name = parts[0];
        const extension = parts[1];
        layoutFileName = layoutPath;
        if (name == 'order') {
            layoutFileName = layoutOrder;
        }
        return new HtmlWebpackPlugin({
            filename: `${name}.html`,
            hash: true,
            template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`),
            layout: layoutFileName,
            replace: templateReplaceAssets,
            inject: false,
            isProduction: isProduction,
        })
    })
}

// авто созадние шаблонов
const htmlPlugins = generateHtmlPlugins('./src/html/views');

module.exports = (env, argv) => {
    return {
        entry: [
            './src/js/index.js',
            './src/less/styles.less',
        ],
        output: {
            filename: './js/bundle.js'
        },
        devtool: argv.mode === 'development' ? "source-map" : "none" ,
        module: {
            rules: [
                {
                    test: /\.js$/,
                    include: path.resolve(__dirname, 'src/js'),
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                },
                {
                    test: /\.less$/,
                    include: path.resolve(__dirname, 'src/less'),
                    use: ExtractTextPlugin.extract({
                        use: [{
                            loader: "css-loader",
                            options: {
                                sourceMap: argv.mode === 'development',
                                minimize: true,
                                url: false
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: [
                                    mqpacker({
                                        sort: sortCSSmq.desktopFirst
                                    }),
                                    cssnano({
                                        preset: ['advanced', {
                                            cssDeclarationSorter: false,
                                            zindex: false,
                                            discardComments: {
                                                removeAll: true
                                            }
                                        }]
                                    })

                                ],
                                sourceMap: argv.mode === 'development',
                            }
                        },
                        {
                            loader: "less-loader",
                            options: {
                                sourceMap: argv.mode === 'development',
                            }
                        }
                        ]
                    })
                },
                {
                    test: /\.html$/,
                    include: path.resolve(__dirname, 'src/html/layouts'),
                    use: ['raw-loader']
                },
            ]
        },
        optimization: argv.mode === "development" ? {} : {
            minimizer: [
                //Incase you want to uglify/minify js
                new UglifyJsPlugin(),
                new OptimizeCssAssetsPlugin({
                    cssProcessorOptions: { discardComments: { removeAll: true } },
                })
            ]
        },
        plugins: [
            new CleanWebpackPlugin(['dist']),
            new ExtractTextPlugin({
                filename: './css/style.bundle.css',
                allChunks: true,
            }),
        ]
            .concat(htmlPlugins) // templates and layouts
            .concat([
                new CtrlwebHtmlWebpackLayoutPlugin(),
                new webpack.ProvidePlugin({
                    '$': 'jquery',
                    jquery: 'jquery',
                    jQuery: 'jquery',
                    'window.jquery': 'jquery',
                    'window.jQuery': 'jquery',
                }),
                new webpack.ProvidePlugin({
                    noUiSlider: 'nouislider',
                }),
            ])
    };
}